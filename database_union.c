//Il vostro codice deve essere pubblicato su un vostro repository gitlab pubblico (è un buon momento per crearsi un account).
//Nel progetto dovra'
//
//esserci un file "database.c" senza main (il main.c lo creo io lato mio) solo con l'implementazione dei metodi.
//Sottomettete il compito anche vuoto e scrivetemi il link al repository in un messagio.
//
//Istruzioni:
//1) Utilizzare la memoria dinamica per salvare i record
//2) gli IndexNode sono i nodi di un albero binario ordinato che serve per trovare i riferimenti ai vari record
//3) Vanno implementate la funzione di inserimento, che aggiorna le varie strutture dati degli indici
//4) Vanno implementate le funzioni di ricerca (che naviga l'opportuno IndexNode per arrivare al record)
//
//Hints:
//- Utilizzare le funzioni di comparazione fra stringhe per navigare l'albero
//- Ispirarsi al codice scritto in classe per gli alberi binari: https://github.com/SENSES-Lab-Sapienza/Lab_Reti/blob/aa_2023_24/Lab/binary-tree.c
//
//Opzionale:
//1) Creazione di un metodo che libera tutta la memoria
//2) Ottimizzare la struttura dati (e mostrare come)
//3) Usare enum per avere un solo tipo di IndexNode che funzioni bene sia con stringhe che con interi (pseudo-polimorfismo)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database_union.h"

//funzione per creare nodo stringa
IndexNode* create_node(value val, Persona * persona){

    //Alloco dinamicamente il nodo
    IndexNode* temp = (IndexNode*) malloc(sizeof(IndexNode));

    if(temp == NULL){
        printf("Errore di allocazione memoria");
        exit(EXIT_FAILURE);
    }

    //Setto le variabili
    temp->val = val;
    temp->persona = persona;
    temp->left = NULL;
    temp->right = NULL;

    return temp;
}

//Funzione per creare database
Database * create_database(){
    
    //Alloco dinamicamente il database
    Database * database = (Database*) malloc(sizeof(Database));
    
    //Inizializzo a NULL le variabili
    database->name = NULL;
    database->surname = NULL;
    database->address = NULL;
    database->age = NULL;

    return database;
}

//Funzione per liberare la memoria dei nodi stringa
void free_database_aux(IndexNode* Node){

    //Navigo all'interno dei nodi
    if(Node->left != NULL){
        free_database_aux(Node->left);
    }

    if(Node->right != NULL){
        free_database_aux(Node->right);
    }

    //Libero il nodo
    free(Node);
    return;
}

//Funzione per liberare la memoria del database
void free_database(Database* database){

    //Libero la memoria di tutti i nodi interni
    free_database_aux(database->name);
    free_database_aux(database->surname);
    free_database_aux(database->address);
    free_database_aux(database->age);

    //Libero la memoria del database
    free(database);
    return;
}

//Funzione per fare la print dell'albero
void print_database(IndexNode * data, int c){

    if(data == NULL){
        return;
    }

    print_database(data->left, c);
    if(c == 0) printf("%s\n", data->val.string);
    else printf("%d\n", data->val.valint);
    print_database(data->right, c);

    return;
}

//Funzione per l'inserimento in ordine del nome
void insert_node(IndexNode * node, value val, int c,  Persona * persona){

    if(node == NULL) return;

    int comparison;

    //controllo il tipo di valore
    if(c == 0){
        comparison = strcmp(node->val.string, val.string);
    } else {
        comparison = node->val.valint - val.valint;
    } 


    //confronto il valore dato con quello al'interno dell'albero
    if(comparison <= 0){

        if(node->right == NULL){

            //Inserisco il nodo all'interno dell'albero
            node->right = create_node(val, persona);
            return;
        }

        insert_node(node->right, val, c, persona);
        return;
    }

    if(node->left == NULL){
        node->left = create_node(val, persona);
        return;
    }

    insert_node(node->left, val, c, persona);
    return;
}

//Funzione insert per inserire i dati della persona
void insert(Database * database, Persona * persona){

    if(persona == NULL || database == NULL) return;    

    //Se il database è nullo lo inizializzo con i dati di persona
    if(database->name == NULL){
        
        database->name = create_node((value) {.string = persona->name}, persona);
        database->surname = create_node((value) {.string = persona->surname}, persona);
        database->address = create_node((value) {.string = persona->address}, persona);
        database->age = create_node((value) {.valint = persona->age}, persona);
        
        return;
    }

    insert_node(database->name, (value) {.string = persona->name}, 0, persona);
    insert_node(database->surname, (value) {.string = persona->surname}, 0, persona);
    insert_node(database->address, (value) {.string = persona->address}, 0, persona);
    insert_node(database->age, (value) {.valint = persona->age}, 1, persona);
    
    return;
}

//Funzione per trovare il nodo stringa
Persona * findByValue_aux(IndexNode * database_value, int c, value val){
    if(database_value == NULL) return NULL;

    int comparison;

    if (c == 0) {
        comparison = strcmp(val.string, database_value->val.string);
    } else {
        comparison = val.valint - database_value->val.valint;
    }

    if(comparison == 0){
        return database_value->persona;
    }

    if(comparison < 0){
        if(database_value->left == NULL) return NULL;
        return findByValue_aux(database_value->left, c, val);
    }

    if(comparison > 0){
        if(database_value->right == NULL) return NULL;
        return findByValue_aux(database_value->right, c, val);
    }

}

Persona* findByName(Database * database, char * name){
    return findByValue_aux(database->name, 0, (value) {.string = name});
}

Persona* findBySurname(Database * database, char * surname){
    return findByValue_aux(database->surname, 0, (value) {.string = surname});
}

Persona* findByAddress(Database * database, char * address){
    return findByValue_aux(database->address, 0, (value) {.string = address});
}

Persona* findByAge(Database * database, int age){
    return findByValue_aux(database->age, 1, (value) {.valint = age});
}