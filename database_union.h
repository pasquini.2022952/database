
// This represent a record in the only schema of this database
typedef struct Persona{
    char name[20];
    char surname[50];
    char address[100];
    int age;
} Persona;

typedef union{
    int valint;
    char * string;
} value;

// This is a node of an index that hold a string
typedef struct IndexNode{
    value val;
    Persona * persona;
    struct IndexNode * left;
    struct IndexNode * right;
} IndexNode;

// A database hold a set of records and a set of indexes
typedef struct {
    IndexNode * name;
    IndexNode * surname;
    IndexNode * address;
    IndexNode * age;
} Database;

// TODO implement the following methods
// The method return a Persona or NULL 

void insert(Database * database, Persona * persona);
Persona* findByName(Database * database, char * name);
Persona* findBySurname(Database * database, char * surname);
Persona* findByAddress(Database * database, char * address);
Persona* findByAge(Database * database, int age);