//Il vostro codice deve essere pubblicato su un vostro repository gitlab pubblico (è un buon momento per crearsi un account).
//Nel progetto dovra'
//
//esserci un file "database.c" senza main (il main.c lo creo io lato mio) solo con l'implementazione dei metodi.
//Sottomettete il compito anche vuoto e scrivetemi il link al repository in un messagio.
//
//Istruzioni:
//1) Utilizzare la memoria dinamica per salvare i record
//2) gli IndexNode sono i nodi di un albero binario ordinato che serve per trovare i riferimenti ai vari record
//3) Vanno implementate la funzione di inserimento, che aggiorna le varie strutture dati degli indici
//4) Vanno implementate le funzioni di ricerca (che naviga l'opportuno IndexNode per arrivare al record)
//
//Hints:
//- Utilizzare le funzioni di comparazione fra stringhe per navigare l'albero
//- Ispirarsi al codice scritto in classe per gli alberi binari: https://github.com/SENSES-Lab-Sapienza/Lab_Reti/blob/aa_2023_24/Lab/binary-tree.c
//
//Opzionale:
//1) Creazione di un metodo che libera tutta la memoria
//2) Ottimizzare la struttura dati (e mostrare come)
//3) Usare enum per avere un solo tipo di IndexNode che funzioni bene sia con stringhe che con interi (pseudo-polimorfismo)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

//funzione per creare nodo stringa
IndexNodeString* create_node_string(char* value, Persona * persona){

    //Alloco dinamicamente il nodo
    IndexNodeString* temp = (IndexNodeString*) malloc(sizeof(IndexNodeString));

    if(temp == NULL){
        printf("Errore di allocazione memoria");
        exit(EXIT_FAILURE);
    }

    //Setto le variabili
    temp->value = value;
    temp->persona = persona;
    temp->left = NULL;
    temp->right = NULL;

    return temp;
}

//funzione per creare nodo intero
IndexNodeInt* create_node_int(int value, Persona * persona){
    
    //Alloco dinamicamente il nodo
    IndexNodeInt* temp = (IndexNodeInt*) malloc(sizeof(IndexNodeInt));

    if(temp == NULL){
        printf("Errore di allocazione memoria");
        exit(EXIT_FAILURE);
    }

    //Setto le variabili
    temp->value = value;
    temp->persona = persona;
    temp->left = NULL;
    temp->right = NULL;

    return temp;
}

//Funzione per creare database
Database * create_database(){
    
    //Alloco dinamicamente il database
    Database * database = (Database*) malloc(sizeof(Database));
    
    //Inizializzo a NULL le variabili
    database->name = NULL;
    database->surname = NULL;
    database->address = NULL;
    database->age = NULL;

    return database;
}

//Funzione per liberare la memoria dei nodi stringa
void free_database_string(IndexNodeString* value){

    //Navigo all'interno dei nodi
    if(value->left != NULL){
        free_database_string(value->left);
    }

    if(value->right != NULL){
        free_database_string(value->right);
    }

    //Libero il nodo
    free(value);
    return;
}

//Funzione per liberare la memoria dei nodi interi
void free_database_int(IndexNodeInt* value){

    //Navigo all'interno dei nodi
    if(value->left != NULL){
        free_database_int(value->left);
    }

    if(value->right != NULL){
        free_database_int(value->right);
    }

    //Libero il nodo
    free(value);
    return;
}

//Funzione per liberare la memoria del database
void free_database(Database* database){

    //Libero la memoria di tutti i nodi interni
    free_database_string(database->name);
    free_database_string(database->surname);
    free_database_string(database->address);
    free_database_int(database->age);

    //Libero la memoria del database
    free(database);
    return;
}

//Funzione per fare la print dell'albero
void print_databse_aux(IndexNodeString * data){

    if(data == NULL){
        return;
    }

    print_databse_aux(data->left);
    printf("%s\n", data->value);
    print_databse_aux(data->right);

    return;
}

//Funzione per l'inserimento in ordine del nome
void insert_string(IndexNodeString * name, char* val, Persona * persona){

    //confronto il nome dato con quello al'interno dell'albero
    if(strcmp(name->value, val) <= 0){

        if(name->right == NULL){

            //Inserisco il nodo all'interno dell'albero
            name->right = create_node_string(val, persona);
            return;
        }

        insert_string(name->right, val, persona);
        return;
    }

    if(name->left == NULL){
        name->left = create_node_string(val, persona);
        return;
    }

    insert_string(name->left, val, persona);
    return;
}


//Funzione per l'inserimento in ordine dell'età
void insert_int(IndexNodeInt * age, int val, Persona * persona){

    if(age->value <= val){

        if(age->right == NULL){
            age->right = create_node_int(val, persona);
            return;
        }

        insert_int(age->right, val, persona);
        return;
    }

    if(age->left == NULL){
        age->left = create_node_int(persona->age, persona);
        return;
    }

    insert_int(age->left, val, persona);
    return;
}

//Funzione insert per inserire i dati della persona
void insert(Database * database, Persona * persona){

    if(persona == NULL || database == NULL) return;

    //Se il database è nullo lo inizializzo con i dati di persona
    if(database->name == NULL){
        
        database->name = create_node_string(persona->name, persona);
        database->surname = create_node_string(persona->surname, persona);
        database->address = create_node_string(persona->address, persona);
        database->age = create_node_int(persona->age, persona);
        
        return;
    }

    insert_string(database->name, persona->name, persona);
    insert_string(database->surname, persona->surname, persona);
    insert_string(database->address, persona->address, persona);
    insert_int(database->age, persona->age, persona);
    
    return;
}

//Funzione per trovare il nodo stringa
Persona * findByValue_aux(IndexNodeString * database_value, char * value){

    if(strcmp(value, database_value->value) == 0){
        return database_value->persona;
    }

    if(strcmp(value, database_value->value) < 0){
        if(database_value->left == NULL) return NULL;
        return findByValue_aux(database_value->left, value);
    }

    if(strcmp(value, database_value->value) > 0){
        if(database_value->right == NULL) return NULL;
        return findByValue_aux(database_value->right, value);
    }

}

//Funzione per trovare il nodo intero
Persona * findByAge_aux(IndexNodeInt * database_value, int age){

    if(database_value->value == age){
        return database_value->persona;
    }

    if(age < database_value->value){
        if(database_value->left == NULL) return NULL;
        return findByAge_aux(database_value->left, age);
    }

    if(age > database_value->value){
        if(database_value->right == NULL) return NULL;
        return findByAge_aux(database_value->right, age);
    }

}

Persona* findByName(Database * database, char * name){
    return findByValue_aux(database->name, name);
}

Persona* findBySurname(Database * database, char * surname){
    return findByValue_aux(database->surname, surname);
}

Persona* findByAddress(Database * database, char * address){
    return findByValue_aux(database->address, address);
}

Persona* findByAge(Database * database, int age){
    return findByAge_aux(database->age, age);
}